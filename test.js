const readline = require("readline")
const solve = require("./index").solve

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: "\nInput board size (n) x n, or (q) to quit: "
})

rl.on("line", (ans) => {
    const answer = ans.trim()
    if (answer === 'q') {
        return rl.close()
    }

    const size = parseInt(answer)
    if (isNaN(size) || size < 1) {
        console.error("Invalid input!")
    } else {
        console.log("Board size:", size, "x", size)
        console.log("Number of solutions (" + size + " x " + size + "):", solve(size))
    }
    rl.prompt()
})

rl.on("close", () => {
    console.log("bye!")
    process.exit(0)
})

rl.prompt()
